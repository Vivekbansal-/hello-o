package com.nearpe.HelloWorld;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {

    @RequestMapping("/hello")
    public String sayHello(){
        System.out.println("Saying hello using Spring Boot...");
        return "hello";
    }
}
